from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.redir_to_feed, name='redir_to_feed'),
    path('admin/', admin.site.urls),
    path('feed/', include('feed.urls')),
    path('account/', include('account.urls')),

    # user login system
    # path('account/login/', 'account.views.login')
]
