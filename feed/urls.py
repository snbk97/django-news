from django.urls import path
from django.conf.urls import url
from . import views

app_name = "feed"
urlpatterns = [
    path('', views.index, name='index'),
    path('details/news<int:news_id>/', views.details, name='details'),
    path('details/news<int:news_id>/comment',
         views.add_comment, name='add_comment'),
    # url(r'^details/news(?P<news_id>[0-9]+)/', views.details, name='redir'),
    path('random', views.randomArticle, name='randomArticle')

]
