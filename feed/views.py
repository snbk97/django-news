from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from django.shortcuts import redirect, render
from django.contrib import auth
from django.contrib.auth.models import User
from django.template.context_processors import csrf
from .models import News, Comments
from .forms import CommentsForm
from django.contrib.auth.decorators import login_required


def index(request):
    news_list = News.objects.all()[:5]
    return render(request, 'feed/index.html', {'news_list': news_list})


def details(request, news_id):
    news_item = get_object_or_404(News, pk=news_id)
    commentform = CommentsForm()
    context = {}
    context.update(csrf(request))
    user = auth.get_user(request)
    context['news_item'] = news_item
    context['comment'] = News.objects.get(pk=news_id).comments_set.all()

    if user.is_authenticated:
        context['form'] = commentform

    return render(request, 'feed/details.html', context)


def randomArticle(request):
    r_article = News.objects.filter(
        pubDate__lte=timezone.now()).order_by('?')[:1][0]
    return render(request, 'feed/random.html', {'news_item': r_article})


@login_required
def add_comment(request, news_id):
    news_item = get_object_or_404(News, pk=news_id)
    form = CommentsForm(request.POST)
    if request.POST:
        if form.is_valid():
            comment = form.save(commit=False)
            comment.user = auth.get_user(request)
            comment.news = News.objects.get(pk=news_id)
            comment.comment = form.cleaned_data['comment']
            comment.save()
        return redirect('feed:details', news_id=news_item.pk)
    else:
        form = CommentsForm()
    return render(request, 'feed/comment.html', {'form': form, 'news_item': news_item})
