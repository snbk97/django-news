from django.db import models
from django.contrib import admin
from django.utils import timezone
from django.contrib.auth.models import User


class NewsAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'pubDate', 'news_date', 'score')
    list_filter = ('pubDate', 'score',)


class News(models.Model):
    class Meta:
        verbose_name_plural = "News"
        ordering = ["-news_date"]

    pubDate = models.DateTimeField(default=timezone.now)
    title = models.CharField('Title', max_length=200)
    data = models.CharField('Data', max_length=400, default='Sample data')
    url = models.CharField('URL', max_length=200)
    news_date = models.CharField('News Date', max_length=10)
    score = models.IntegerField('Point', default=0)
    # post_comments = models.IntegerField('Comments', default=0)

    def __str__(self):
        return str(self.title)


class CommentsAdmin(admin.ModelAdmin):
    list_display = ('news', '__str__', 'user', 'votes')


class Comments(models.Model):
    class Meta:
        verbose_name_plural = "Comments"

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    comment = models.CharField(max_length=1000)
    date = models.DateTimeField(default=timezone.now)
    news = models.ForeignKey(News, on_delete=models.CASCADE)
    votes = models.IntegerField(default=0)
    # Score = models.IntegerField(default=0)

    def __str__(self):
        return self.comment
