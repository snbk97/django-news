from django.contrib import admin
from .models import News, NewsAdmin, Comments, CommentsAdmin

admin.site.register(News, NewsAdmin)
admin.site.register(Comments, CommentsAdmin)
