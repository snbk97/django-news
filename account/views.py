from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect, reverse
from django.http import HttpResponse
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .forms import UserForm
from .forms import UserProfileForm
from .forms import LoginForm


def success(request):
    return render(request, 'account/signup_success.html', {})


def redir_main(request):
    return redirect('account:signup')


@login_required
def logout_view(request):
    logout(request)
    return redirect('feed:index')


@login_required
def profile_view(request):
    return render(request, 'account/profile.html')


def login_view(request):
    form = LoginForm(request.POST)
    if request.POST:
        if form.is_valid():
            uname = form.cleaned_data['username']
            pwd = form.cleaned_data['password']
            user = authenticate(request, username=uname, password=pwd)
            if user is not None:
                login(request, user)
                # return render(request, "/feed/index.html", {'user': user})
                return redirect('feed:index')
        else:
            return HttpResponse("Invalid Credentials")
    else:
        form = LoginForm()

    return render(request, 'account/login.html', {'form': form})


def register(request):
    if request.POST:
        uForm = UserForm(request.POST)
        upForm = UserProfileForm(request.POST)
        if uForm.is_valid() and upForm.is_valid():
            user = uForm.save()
            profile = upForm.save(commit=False)
            profile.user = user
            profile.bio = upForm.cleaned_data['bio']
            profile.save()
            messages.success(request, "Account Created")
            return render(request, 'account/signup_success.html', {})

        else:
            messages.error(request, "Error Occured")
            return render(request, 'account/signup.html',
                          {
                              'uForm': UserForm(request.POST),
                              'upForm': UserProfileForm(request.POST)

                          })

    else:
        uForm = UserForm()
        upForm = UserProfileForm()

    return render(request, 'account/signup.html',
                  {
                      'uForm': uForm,
                      'upForm': upForm
                  })
