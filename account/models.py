from django.db import models
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import post_save


class UserProfile(models.Model):
    GENDER_CHOICE = (
        ('M', 'Male'),
        ('F', 'Female'),
        ('O', 'Other')
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE,
                                related_name='uprofile')
    gender = models.CharField(max_length=6, choices=GENDER_CHOICE,
                              default='Male')
    bio = models.CharField(max_length=300, blank=True, null=True)

    # -------- PREFERENCES -------- #
    email_notif = models.BooleanField(default=True)
    dark_mode = models.BooleanField(default=False)

    def __str__(self):
        return str(self.user)
