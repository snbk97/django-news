from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.forms import ModelForm
from django import forms

from .models import UserProfile


class UserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2',)
        help_texts = {
            'username': None,
            'password1': None,
            'password2': None,
        }

    def save(self, commit=True):
        user = super(UserForm, self).save(commit=False)
        user.is_staff = True

        if commit:
            user.save()

        return user


class UserProfileForm(ModelForm):
    class Meta():
        model = UserProfile
        exclude = ('user',)


class LoginForm(forms.Form):
    username = username = forms.CharField(label='username', max_length=100)
    password = forms.CharField(widget=forms.PasswordInput())
