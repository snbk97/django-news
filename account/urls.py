from django.urls import path
from django.conf.urls import url


from . import views

app_name = "account"
urlpatterns = [
    path('', views.redir_main, name='redirect_main'),
    path('signup/', views.register, name='signup'),
    path('signup_success/', views.success, name='signup_success'),

    # login
    path('login/', views.login_view, name='login'),
    path('logout/', views.logout_view, name='logout'),
    path('profile/', views.profile_view, name='profile')

]
